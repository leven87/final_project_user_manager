package com.dtuPay.usermanager.db;

import com.dtuPay.usermanager.system.Customer;
import com.dtuPay.usermanager.system.Merchant;

/**
 * A repository for all users registered in DTUPay.
 * 
 * Extend this interface to implement concrete storage options e.g. a database.
 * 
 * @author Anelia, Li Wang, Chuan
 *
 */
public interface UserRepository {
    
    /**
     * Creates a new customer in the system 
     * and stores their information to the repository.
     * 
     * @param cpr customer ID (must not be already contained)
     * @param firstName first name of the new customer
     * @param lastName surname of the new customer
     * @param account bank account ID of the customer
     * 
     * @return true if the customer was successfully registered
     *          false otherwise
     */
    public boolean registerCustomer(String cpr, String firstName, String lastName, String account);
    
    /**
     * Creates a new merchant in the system 
     * and stores their information to the repository.
     * 
     * @param cpr merchant ID (must not be already contained)
     * @param firstName first name of the new merchant
     * @param lastName surname of the new merchant
     * @param account bank account ID of the merchant
     * 
     * @return true if the merchant was successfully registered
     *          false otherwise
     */
    public boolean registerMerchant(String cpr, String firstName, String lastName, String account);
    
    /**
     * @param cpr customer ID
     * @return a Customer object containing all stored information for this ID.
     */
    public Customer getCustomerInformation(String cpr);
    
    /**
     * @param cpr merchant ID
     * @return a Merchant object containing all stored information for this ID.
     */
    public Merchant getMerchantInformation(String cpr);
    
    /**
     * Whether there exists a registered customer with this ID.
     * 
     * @param cpr customer ID
     * @return true if a customer with this CPR is stored in the system,
     *          false otherwise
     */
    public boolean customerExists(String cpr);
    
    /**
     * Whether there exists a registered merchant with this ID.
     * 
     * @param cpr merchant ID
     * @return true if a merchant with this CPR is stored in the system,
     *          false otherwise
     */
    public boolean merchantExists(String cpr);
    
}
