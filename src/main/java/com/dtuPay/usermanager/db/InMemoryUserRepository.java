package com.dtuPay.usermanager.db;

import java.util.HashMap;
import java.util.Map;

import com.dtuPay.usermanager.system.Customer;
import com.dtuPay.usermanager.system.Merchant;

/**
 * A repository of DTU Users implemented using hash maps.
 * 
 * The data in this repository is only kept until the server is stopped.
 * This instance is appropriate for system tests.
 * For more persistent options, use file storage or a database.
 * 
 * @author Anelia, Rui, Li Wang, Chuan
 *
 */
public class InMemoryUserRepository implements UserRepository {
    
    /**
     * All customers registered in the system identified by their CPR number.
     */
    private Map<String, Customer> customerList = new HashMap<>();
    
    /**
     * All merchants registered in the system identified by their CPR/CVR number.
     */
    private Map<String, Merchant> merchantList = new HashMap<>();

    @Override
    public boolean registerCustomer(String cpr, String firstName, String lastName, String account) {
        if (customerList.containsKey(cpr)) {
            return false;
        }
        
        Customer newCustomer = new Customer(cpr, firstName, lastName, account);
        customerList.put(cpr, newCustomer);
        return true;
    }
    
    @Override
    public boolean registerMerchant(String cpr, String firstName, String lastName, String account) {
        if (merchantList.containsKey(cpr)) {
            return false;
        }
        
        Merchant newMerchant = new Merchant(cpr, firstName, lastName, account);
        merchantList.put(cpr, newMerchant);
        return true;
    }
    
    @Override
    public Customer getCustomerInformation(String cpr) {
        if (!customerList.containsKey(cpr)) {
            return null;
        }
        
        return customerList.get(cpr);
    }

    @Override
    public Merchant getMerchantInformation(String cpr) {
        if (!merchantList.containsKey(cpr)) {
            return null;
        }
        
        return merchantList.get(cpr);
    }

    @Override
    public boolean customerExists(String cpr) {
        return customerList.containsKey(cpr);
    }

    @Override
    public boolean merchantExists(String cpr) {
        return merchantList.containsKey(cpr);
    }

}
