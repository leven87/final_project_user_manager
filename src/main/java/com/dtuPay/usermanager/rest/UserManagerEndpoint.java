package com.dtuPay.usermanager.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import com.dtuPay.usermanager.system.*;
import com.dtuPay.usermanager.db.InMemoryUserRepository;
import com.dtuPay.usermanager.rest.dto.*;

/**
 * REST Interface for User Manager
 * 
 * 
 * @author Anelia, Li Wang, Chuan, Bijan
 *
 */

/* Note that the resource names and the exclusive use of HTTP POST is on purpose.
 * You should be designing the right resource URI's and use the correct HTTP verb yourself.
 */
@Path("/")
public class UserManagerEndpoint {
	
	private static UserManager userManager = new UserManager(new InMemoryUserRepository());
	
	@POST
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerCustomer(UserRepresentation c) {
		userManager.registerCustomer(c.getCpr(),
		        c.getFirstName(),
		        c.getLastName(),
		        c.getAccount());
		return c.getCpr();
	}
	
	
	@POST
	@Path("merchants")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerMerchant(UserRepresentation m) {
		userManager.registerMerchant(m.getCpr(),
		        m.getFirstName(),
		        m.getLastName(),
		        m.getAccount());
		return m.getCpr();
	}
	
	@GET
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public Boolean validateCustomer(UserRepresentation c) {
		return userManager.customerExists(c.getCpr());
	}	
	
	@GET
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public Boolean validateMerchant(UserRepresentation m) {
		return userManager.merchantExists(m.getCpr());
	}	
	
	@GET
	@Path("customers/{userId}")
    @Produces("application/json")
	public UserRepresentation getCustomerInformation(@PathParam("userId") String userId) {
	    return userManager.getCustomerInformation(userId);
	}
	
	@GET
    @Path("merchants/{userId}")
    @Produces("application/json")
    public UserRepresentation getMerchantInformation(@PathParam("userId") String userId) {
        return userManager.getMerchantInformation(userId);
    }
}
