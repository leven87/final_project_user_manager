package com.dtuPay.usermanager.system;

/**
 * Class representation of a DTUPay user
 * @author Anelia, Li Wang, Chuan
 *
 */
public abstract class DtuPayUser {

    /**
     * First name of the user.
     */
    private String firstName;
    
    /**
     * Surname of the user.
     */
    private String lastName;
    
    /**
     * User indentifier (CPR or CVR number).
     */
    private String cpr;
    
    /**
     * Bank account of the user.
     */
    private String account;
    
    public DtuPayUser(String cpr, String firstName, String lastName, String account) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
        this.account = account;
    }
    
    public String getCpr() {
        return cpr;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public String getAccount() {
        return account;
    }
}

