package com.dtuPay.usermanager.system;

import  com.dtuPay.usermanager.db.*;
import com.dtuPay.usermanager.rest.dto.UserRepresentation;

/**
 * A class for managing user information like bank account.
 * 
 * @author  Li Wang, Anelia, Chuan
 *
 */
public class UserManager {
    
    public UserManager(UserRepository repository) {
        this.repository = repository;
    }
    
    private UserRepository repository;
    
    /**
     * Registers a new customer to DTUPay.
     * 
     * @param cpr identifier of the customer (must not be already in the system)
     * @param firstName first name of the customer
     * @param lastName surname of the customer
     * @param account bank account of the customer
     * @return true if the customer was successfully registered,
     *          false otherwise
     */
    public boolean registerCustomer(String cpr, String firstName, String lastName, String account)  {
    	if (repository.customerExists(cpr)) {
    		return false;
    	}
    	
    	repository.registerCustomer(cpr, firstName, lastName, account);
    	return true;	
    }

    /**
     * Registers a new merchant to DTUPay.
     * 
     * @param cpr identifier of the merchant (must not be already in the system)
     * @param firstName first name of the merchant
     * @param lastName surname of the merchant
     * @param account bank account of the merchant
     * @return true if the merchant was successfully registered,
     *          false otherwise
     */
    public boolean registerMerchant(String cvr, String firstName, String lastName, String account)  {        
    	if (repository.merchantExists(cvr)) {
    		return false;
    	}    
    	
    	repository.registerMerchant(cvr, firstName, lastName, account);
    	return true;	
    }

    /**
     * @param cpr identifier of the customer
     * @return true if the customer is already registered in the system,
     *          false otherwise
     */
	public boolean customerExists(String cpr) {
    	return repository.customerExists(cpr);
	}

	/**
     * @param cvr identifier of the merchant
     * @return true if the merchant is already registered in the system,
     *          false otherwise
     */
	public boolean merchantExists(String cvr) {
    	return repository.merchantExists(cvr);
	}
	
	/**
	 * Returns customer information that can be later transferred through REST.
	 * 
	 * @param cpr customer ID in question
	 * @return representation of the customer
	 */
	public UserRepresentation getCustomerInformation(String cpr) {
	    if (!repository.customerExists(cpr)) {
	        return null;
	    }
	    
        Customer customer = repository.getCustomerInformation(cpr);
        
	    UserRepresentation customerRepresentation = new UserRepresentation();
	    customerRepresentation.setFirstName(customer.getFirstName());
	    customerRepresentation.setLastName(customer.getLastName());
	    customerRepresentation.setCpr(customer.getCpr());
	    
	    return customerRepresentation;
	}
	
	/**
	 * Returns merchant information that can be later transferred through REST.
	 * 
	 * @param cvr merchant ID in question
	 * @return representation of the merchant
	 */
	public UserRepresentation getMerchantInformation(String cvr) {
	    if (!repository.merchantExists(cvr)) {
            return null;
        }
	    
	    Merchant merchant = repository.getMerchantInformation(cvr);
	    
	    UserRepresentation merchantRepresentation = new UserRepresentation();
        merchantRepresentation.setFirstName(merchant.getFirstName());
        merchantRepresentation.setLastName(merchant.getLastName());
        merchantRepresentation.setCpr(merchant.getCpr());
        
        return merchantRepresentation;
	}
}
