package behaviourtests;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.dtuPay.usermanager.system.*;
import com.dtuPay.usermanager.db.InMemoryUserRepository;
import com.dtuPay.usermanager.rest.dto.*;

public class UserSteps {
	
	private UserManager userManager = new UserManager(new InMemoryUserRepository());
	private UserRepresentation customer = new UserRepresentation();
	private UserRepresentation merchant = new UserRepresentation();

		
	@Given("^a customer with a bank account$")
	public void aCustomerWithABankAccount() throws Throwable {
		customer.setCpr("ffff");
		customer.setFirstName("li");
		customer.setLastName("zihao");
		customer.setAccount("1234");
	}

	@When("^the customer register in dtuPay$")
	public void theCustomerRegisterInDtuPay() throws Throwable {
		userManager.registerCustomer(customer.getCpr(), 
				customer.getFirstName(), 
				customer.getLastName(),
				customer.getAccount());
	}

	@Then("^the customer is registered successfully$")
	public void theCustomerIsRegisteredSuccessfully() throws Throwable {
		assertTrue(userManager.customerExists(customer.getCpr()));
	}
	
	@Given("^a merchant with a bank account$")
	public void aMerchantWithABankAccount() throws Throwable {
		merchant.setCpr("ffff_merchant");
		merchant.setFirstName("li_merchant");
		merchant.setLastName("zihao_merchant");
		merchant.setAccount("4321");
	}

	@When("^the merchant register in dtuPay$")
	public void theMerchantRegisterInDtuPay() throws Throwable {
		userManager.registerMerchant(merchant.getCpr(), 
				merchant.getFirstName(), 
				merchant.getLastName(),
				merchant.getAccount());
	}

	@Then("^the merchant is registered successfully$")
	public void theMerchantIsRegisteredSuccessfully() throws Throwable {
		assertTrue(userManager.merchantExists(merchant.getCpr()));
	}	
}
