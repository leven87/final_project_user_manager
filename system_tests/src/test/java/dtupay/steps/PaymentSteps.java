package dtupay.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import dtupay.rest.dto.Token;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PaymentSteps {
	private BankService bank = new BankServiceService().getBankServicePort();
	private List<String> accounts = new ArrayList<>();
	private DtuPay dtuPay = new DtuPay(bank);
	private List<Token> tokens;
	private Token merchantToken;
	private boolean success;
	private BigDecimal customerBalance;
	private BigDecimal merchantBalance;
	private int amount;
	private String customerCprNumber;
	private String merchantCprNumber;
	private String customerAccount;
	private String merchantAccount;
	
	@After
	public void cleanupUsedAccounts() throws BankServiceException_Exception {
		for (String a : accounts) {
			bank.retireAccount(a);
		}
	}
	
	@Given("a registered customer with a bank account")
	public void aRegisteredCustomerWithABankAccount() throws Throwable {
		User u = new User();
		customerCprNumber = "280980-0728fds";
		u.setCprNumber(customerCprNumber);
		u.setFirstName("Sofia23");
		u.setLastName("Frandsen23");
		customerBalance = new BigDecimal(1000);
		customerAccount = bank.createAccountWithBalance(u, customerBalance);
		accounts.add(customerAccount); // Remember created accounts for cleanup
		dtuPay.registerCustomer(u.getCprNumber()
				,u.getFirstName()
				,u.getLastName()
				,customerAccount);
	}
	
//	@Given("a registered merchant with a bank account")
//	public void aRegisteredMerchantWithABankAccount() throws Throwable {
//		User u = new User();
//		u.setCprNumber("290185-1894");
//		u.setFirstName("Claudia");
//		u.setLastName("Parker");
//		merchantBalance = new BigDecimal(1000);
//		merchantAccount = bank.createAccountWithBalance(u, merchantBalance);
//		accounts.add(merchantAccount); // remember created accounts for cleanup
//		dtuPay.registerMerchant(u.getCprNumber(),u.getFirstName(),u.getLastName(),merchantAccount);
//	}
//	
//	@Given("the customer has one unused token")
//	public void theCustomerHasOneUnusedToken() throws Throwable {
//		tokens = dtuPay.requestTokens(customerCprNumber,1);
//		assertTrue(tokens.size() >= 1);
//	}
//	
//	@When("the merchant scans the token")
//	public void theMerchantScansTheToken() throws Throwable {
//		merchantToken = tokens.get(0);
//	}
//	
//	@When("requests payment for {int} kroner using the token")
//	public void requestsPaymentForKronerUsingTheToken(int amt) throws Throwable {
//		amount = amt;
//	    success = dtuPay.payWithToken(amount,merchantCprNumber,merchantToken,"some description");
//	}
//	
//	@Then("the payment succeeds")
//	public void thePaymentSucceeds() throws Throwable {
//	    assertTrue(success);
//	}
//	
//	@Then("the money is transferred from the customer bank account to the merchant bank account")
//	public void theMoneyIsTransferredFromTheCustomerBankAccountToTheMerchantBankAccount() throws Throwable {
//		assertEquals(customerBalance.subtract(new BigDecimal(amount)),bank.getAccount(customerAccount).getBalance());
//		assertEquals(merchantBalance.add(new BigDecimal(amount)),bank.getAccount(merchantAccount).getBalance());
//	}
	
}

