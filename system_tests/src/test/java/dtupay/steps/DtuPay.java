package dtupay.steps;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import dtu.ws.fastmoney.BankService;
import dtupay.rest.dto.DtuPayUserRepresentation;
import dtupay.rest.dto.PaymentRequest;
import dtupay.rest.dto.Token;
import dtupay.rest.dto.TokenRequest;

public class DtuPay {

	WebTarget baseUrl;
	
	public DtuPay(BankService bank) {
		Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
	}

	public void registerCustomer(String cpr
			,String firstName
			,String lastName
			,String account) {
		DtuPayUserRepresentation customer = new DtuPayUserRepresentation();
		customer.setCpr(cpr);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAccount(account);
		baseUrl.path("somepath")
		.path("more1")
		.request()
		.post(Entity.entity(customer, "application/json"));
	}

	public void registerMerchant(String cpr
			,String firstName
			,String lastName
			,String account) {
		DtuPayUserRepresentation merchant = new DtuPayUserRepresentation();
		merchant.setCpr(cpr);
		merchant.setFirstName(firstName);
		merchant.setLastName(lastName);
		merchant.setAccount(account);
		baseUrl.path("somepath")
		.path("more2")
		.request()
		.post(Entity.entity(merchant, "application/json"));
	}

	public List<Token> requestTokens(String cprNumber, int i) {
		TokenRequest tr = new TokenRequest();
		tr.setCpr(cprNumber);
		tr.setNumber(i);
		Token[] tokens = baseUrl.path("somepath")
		.path("more3")
		.request().post(Entity.entity(tr, "application/json"),Token[].class);
		return Arrays.asList(tokens);
	}

	public boolean payWithToken(int amount, String cprNumber, Token token, String description) {
		PaymentRequest pr = new PaymentRequest();
		pr.setAmount(amount);
		pr.setMerchantCpr(cprNumber);
		pr.setToken(token);
		pr.setDescription(description);
		boolean result =
				baseUrl.path("somepath")
				.path("more4")
				.request()
				.post(Entity.entity(pr, "application/json"),Boolean.class);
		
		return result;
	}
}
