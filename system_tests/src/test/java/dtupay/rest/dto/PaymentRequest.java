package dtupay.rest.dto;

public class PaymentRequest {
	private int amount;
	private String merchantCpr;
	private Token token;
	private String description;
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getMerchantCpr() {
		return merchantCpr;
	}
	public void setMerchantCpr(String merchantCpr) {
		this.merchantCpr = merchantCpr;
	}
	public Token getToken() {
		return token;
	}
	public void setToken(Token token) {
		this.token = token;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
