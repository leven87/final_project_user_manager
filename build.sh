#!/bin/bash
set -e
#mvn clean package
#docker-compose up --build final_project_user_manager


#pushd  final_project_user_manager

mvn clean package

#stop and delete running containers
docker ps -q --filter ancestor="final_project_user_manager" | xargs -r docker stop
docker container prune
docker image prune -f 

# Build the docker image using
# demo/Dockerfile
docker build --tag final_project_user_manager .
docker run -p 8085:8080 final_project_user_manager&

# Do a garbage collection of images not used anymore.
docker container prune
docker image prune -f 

mvn clean test
#popd