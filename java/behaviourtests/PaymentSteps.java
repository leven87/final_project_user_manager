package behaviourtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import messaging.Event;
import messaging.EventSender;
import payment.PaymentService;

public class PaymentSteps {
	String account1, account2;
	EventSender eventSender;
	PaymentService paymentService;
	CompletableFuture<Boolean> response = new CompletableFuture<Boolean>();
	BigDecimal amount;
	CompletableFuture<Event> sentEvent = new CompletableFuture<Event>();
	
	public PaymentSteps() {
		eventSender = new EventSender() {
			@Override
			public void sendEvent(Event event) {
				sentEvent.complete(event);
			}
		};
		paymentService = new PaymentService(eventSender);
	}

	@Given("^a customer with a bank account$")
	public void aCustomerWithABankAccount() throws Throwable {
		account1 = "account1";
	}

	@Given("^a second customer with a bank account$")
	public void aSecondCustomerWithABankAccount() throws Throwable {
		account2 = "account2";
	}

	@When("^the first customer transfers money to the second customer$")
	public void theFirstCustomerTransfersMoneyToTheSecondCustomer() throws Throwable {
		amount = new BigDecimal(100);
		new Thread(() -> {
			try {
				response.complete(paymentService.requestPayment(account1, account2, amount,"Comment"));
			} catch (Exception e) {
				throw new Error(e);
			}
		}).start();
	}

	@Then("^the \"([^\"]*)\" event is broadcast$")
	public void theEventIsBroadcast(String event) throws Throwable {
		assertEquals(event,sentEvent.join().getEventType());
	}
	
	@When("^the event \"([^\"]*)\" is received$")
	public void theEventIsReceived(String event) throws Throwable {
		Event e = new Event(event);
		paymentService.receiveEvent(e);
	}

	@Then("^the payment succeeds$")
	public void thePaymentSucceeds() throws Throwable {
		assertTrue(response.join());
	}
}
